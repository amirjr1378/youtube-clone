import { useLocalStorage } from "@greysonevins/use-local-storage";
import { useRouter } from "next/router";
import axios from "axios";
import Cookies from "js-cookie";
import { removeCookie, getCookie } from "../helpers/cookie";
import { getMe } from "../api/auth";
import { setAxiosToken } from "../axios";

export const userContext = React.createContext({
  token: "",
  user: {},
  login: () => {},
  logout: () => {},
});

export function UserProvider({ children }) {
  const [state, setState] = useLocalStorage("user", {});
  const router = useRouter();

  function login(token) {
    const AUTH_TOKEN = `Bearer ${token}`;
    axios.defaults.headers.common["Authorization"] = AUTH_TOKEN;
    //set cookie
    Cookies.set("token", token);

    setState((state) => ({ ...state, token }));
    router.push("/");
  }

  function logout() {
    setState({});
    removeCookie("token");
    router.push("/login");
  }
  function getState() {
    return {
      ...state,
      login,
      logout,
    };
  }

  return (
    <userContext.Provider value={getState()}>{children}</userContext.Provider>
  );
}
