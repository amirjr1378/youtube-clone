import { useState } from "react";
import { createContext } from "react";

export const sidenavContext = createContext({
  open: true,
  toggle: () => {},
});

export function SidenavProvider({ children }) {
  const [open, setOpen] = useState(false);

  const toggle = function (toggle) {
    setOpen((state) => !state);
  };

  const getValues = function () {
    return {
      open,
      toggle,
    };
  };
  return (
    <sidenavContext.Provider value={getValues()}>
      {children}
    </sidenavContext.Provider>
  );
}
