import { useState } from "react";
import Axios from "axios";
import UploadImage from "./UploadImage";

export default function UploadThumbnail({ callback, label, id }) {
  const fd = new FormData();
  const [progress, setProgress] = useState(0);

  const handleUpload = async function (file) {
    fd.append("thumbnail", file);

    const config = {
      onUploadProgress: function (progressEvent) {
        var percentCompleted = Math.round(
          (progressEvent.loaded * 100) / progressEvent.total
        );
        setProgress(percentCompleted);
      },
    };
    const url = `/videos/${id}/thumbnails`;

    try {
      const res = await Axios.put(url, fd, config);
      if (res.data.success) {
        callback && callback();
      }
    } catch (e) {
      console.log("error", e);
    } finally {
      setProgress(0);
    }
  };
  return (
    <UploadImage
      name="thumbnail"
      callback={handleUpload}
      progress={progress}
      label={label}
    />
  );
}
