import { Button } from "@material-ui/core";

export default function LoadingButton({ children, loading, ...props }) {
  return (
    <Button {...props} disabled={loading}>
      {loading ? "loading..." : children}
    </Button>
  );
}
