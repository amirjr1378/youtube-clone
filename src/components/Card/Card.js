function Card({ children, title }) {
  return (
    <div className="card">
      {title && <div className="title">{title}</div>}
      {children}

      <style jsx>{`
        .card {
          border: 1px solid var(--divider);
          border-radius: 8px;
          display: flex;
          flex-direction: column;
          font-size: 1rem;
          background: white;
          min-height: 10rem;
        }
        .title {
          padding: 0.8em;
          font-size: 1.3em;
          font-weight: bold;
          border-bottom: 1px solid var(--divider);
        }
      `}</style>
    </div>
  );
}

export default Card;
