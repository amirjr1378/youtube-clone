function CardBody({ children }) {
  return (
    <div className="card__body">
      {children}

      <style jsx>{`
        .card__body {
          padding: 1em 0.9em;
          display: flex;
          flex-direction: column;
          justify-content: space-between;
          height: 100%;
          width: 100%;
          flex-grow: 1;
        }
      `}</style>
    </div>
  );
}

export default CardBody;
