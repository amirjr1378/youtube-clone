import { makeThumbnailRoute } from "../helpers/staticsRoute";
import { truncate } from "../helpers/truncate";
import moment from "moment";

export default function SearchVideo({
  title,
  thumbnailUrl,
  userId = { photoUrl: "", username: "" },
  views,
  createdAt,
  description,
}) {
  return (
    <div className="search-video">
      <div
        className="search-video__thumbnail"
        style={{
          backgroundImage: `url('${makeThumbnailRoute(thumbnailUrl)}')`,
        }}
      />

      <div className="search-video__description">
        <h4>{title}</h4>
        <div className="text--muted">
          <div>
            {userId.username} {views} views , {moment(createdAt).fromNow()}
          </div>
          {truncate(description, 70)}
        </div>
      </div>

      <style jsx>{`
        .search-video {
          display: flex;
          align-items: center;
          border-bottom: 1px solid var(--divider);
          margin-bottom: 20px;
          font-size: 1rem;
          padding: 0.9em 0.3em;
        }

        .search-video__thumbnail {
          width: 250px;
          height: 140px;
          background-size: cover;
          background-position: center;
        }

        .search-video__description {
          margin-left: 5em;
          flex-grow: 1;
        }

        .text--muted {
          font-size: 1em;
          color: var(--text-muted);
        }
      `}</style>
    </div>
  );
}
