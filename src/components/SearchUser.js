import { makeAvatarRoute } from "../helpers/staticsRoute";
import SubscribeButton from "./SubscribeButton";
import moment from "moment";

export default function SearchUser({
  channelName,
  photoUrl,
  id,
  subscribers,
  videos,
  email,
  createdAt,
}) {
  return (
    <div className="search-user">
      <div className="search-user__image-container">
        <div
          className="search-user__image"
          style={{ backgroundImage: `url('${makeAvatarRoute(photoUrl)}')` }}
        />
      </div>
      <div className="search-user__description">
        <h4>{channelName}</h4>
        <div className="text--muted">
          <div>
            {subscribers} subscribers | {videos} videos
          </div>
          <div>
            {email} member since {moment(createdAt).fromNow()}
          </div>
        </div>
      </div>

      <SubscribeButton channelId={id}>Subscribe</SubscribeButton>

      <style jsx>{`
        .search-user {
          display: flex;
          align-items: center;
          border-bottom: 1px solid var(--divider);
          margin-bottom: 20px;
          font-size: 1rem;
          padding: 0.9em 0.3em;
        }

        .search-user__image {
          width: 100px;
          height: 100px;
          border-radius: 100%;
          background-size: cover;
          background-position: center;
        }
        .search-user__image-container {
          width: 250px;
          height: 140px;
          display: flex;
          justify-content: center;
          align-items: center;
        }

        .search-user__description {
          margin-left: 5em;
          flex-grow: 1;
        }

        .text-muted {
          font-size: 1em;
          color: var(--text-muted);
        }
      `}</style>
    </div>
  );
}
