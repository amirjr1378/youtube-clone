import { truncate } from "../helpers/truncate";
import { Icon } from "@material-ui/core";
import { makeThumbnailRoute, makeAvatarRoute } from "../helpers/staticsRoute";
import moment from "moment";
import { useRouter } from "next/router";

const videoPageBaseUrl = "/video";

export default function VideoPriview({
  title,
  thumbnailUrl,
  userId = { photoUrl: "", username: "" },
  views,
  createdAt,
  className,
  onClick,
  _id,
  ...props
}) {
  const router = useRouter();
  function goToVideoPage() {
    onClick && onClick();
    router.push(`${videoPageBaseUrl}/${_id}`);
  }
  return (
    <div
      className={`preview ${className || ""}`}
      {...props}
      onClick={goToVideoPage}
    >
      <div className="preview__image" />
      <div className="margin-top--3">
        <div className="priview__title">
          <img src={makeAvatarRoute(userId.photoUrl)} />
          <h3 className="priview__title-text">{truncate(title, 50)}</h3>
        </div>
        <div className="preview__text--muted">
          <div>{userId.username}</div>
          <div>
            {views} views
            <Icon
              style={{
                fontSize: 5,
                marginBottom: 5,
                marginLeft: 3,
                marginRight: 3,
              }}
            >
              fiber_manual_record
            </Icon>
            <span className="preview__date">{moment(createdAt).fromNow()}</span>
          </div>
        </div>
      </div>

      <style jsx>{`
        .preview {
          display: flex;
          flex-direction: column;
          cursor: pointer;
        }
        .margin-top--3 {
          margin-top: 1rem;
        }
        .preview__image {
          width: 100%;
          height: 255px;
          background-image: url('${makeThumbnailRoute(thumbnailUrl)}');
          background-color: lightgray;
          background-size: cover;
          background-repeat: no-repeat;
          background-position: center;
        }
        .priview__title {
          display: flex;
        }
        .priview__title img {
          width: 36px;
          height: 36px;
          border-radius: 100%;
        }

        .priview__title h3 {
          padding-left: 0.2rem;
          font-size: 1rem;
          line-height: 1.5rem;
          max-height: 4rem;
        }

        .preview__text--muted {
          margin-top: 0.3rem;
          margin-left: 38px;
          font-size: 1rem;
          color: rgb(96, 96, 96);
        }
      `}</style>
    </div>
  );
}
