import { Icon } from "@material-ui/core";
export default function UploadImage({
  callback,
  progress = 0,
  name,
  label = "Upload image",
}) {
  const inputRef = React.createRef();
  const openPicker = () => inputRef.current && inputRef.current.click();

  const handleChange = async function (e) {
    const file = e.target.files[0];
    callback && callback(file);
  };
  return (
    <div className="thumbnail" onClick={openPicker}>
      <div className="progress" style={{ width: progress + "%" }}></div>
      <input
        ref={inputRef}
        type="file"
        name={name}
        id={"id-" + name}
        className="thumbnail__input"
        onChange={handleChange}
      />
      <div className="thubmnail__icon">
        <Icon>collections</Icon>
      </div>
      <p className="thumbnail__label">{label}</p>

      <style jsx>{`
        .thumbnail {
          border: 1px dashed lightgray;
          display: flex;
          flex-direction: column;
          justify-content: center;
          height: 80px;
          position: relative;
          margin: 1rem;
          cursor: pointer;
          color: gray;
          text-align: center;
        }

        .thumbnail__input {
          display: none;
        }

        .thumbnail__label {
          font-size: 0.9rem;
        }
        .progress {
          height: 100%;
          position: absolute;
          left: 0;
          top: 0;
          background: rgba(100, 90, 83, 0.4);
        }
      `}</style>
    </div>
  );
}
