import { useState } from "react";
import { Button, Icon } from "@material-ui/core";
import { toggleSubscription, checkIsSubscribeTo } from "../api/subscriptions";

export default function SubscribeButton({
  children,
  onClick,
  channelId,
  ...props
}) {
  const [isSub, setIsSub] = useState(false);

  const handleClick = async function () {
    const success = await toggleSubscription(channelId);
    setIsSub(!!success);
    onClick && onClick();
  };

  React.useEffect(() => {
    checkIsSubscribeTo(channelId).then((success) => setIsSub(success));
  }, []);
  return (
    <Button
      variant="contained"
      color={isSub ? "" : "secondary"}
      {...props}
      onClick={handleClick}
    >
      {children}
      {isSub && (
        <span style={{ marginLeft: 5 }}>
          <Icon>done</Icon>
        </span>
      )}
    </Button>
  );
}
