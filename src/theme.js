import { createMuiTheme } from "@material-ui/core/styles";
import { red } from "@material-ui/core/colors";

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#065FD4",
    },
    secondary: {
      main: "#ff0000",
    },
    error: {
      main: red.A400,
    },
  },
});

export default theme;
