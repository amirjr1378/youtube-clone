import axios from "axios";

export function search(text) {
  if (!text) return;
  return axios.post("/search", { text }).then((res) => res.data.data);
}
