import axios from "axios";
import { setAxiosToken } from "../axios";
import { getCookie } from "../helpers/cookie";

export function toggleSubscription(channelId) {
  setAxiosToken(getCookie("token"));
  return axios
    .post("/subscriptions", { channelId })
    .then((res) => res.data.success);
}

export function checkIsSubscribeTo(channelId) {
  setAxiosToken(getCookie("token"));
  return axios
    .post("/subscriptions/check", { channelId })
    .then((res) => res.data.success);
}
