import axios from "axios";
import { setAxiosToken } from "../axios";
import { getCookie } from "../helpers/cookie";

export function getSubVideos() {
  return axios.get("/subscriptions/videos").then((res) => res.data.data || []);
}

export function getAllVideos() {
  return axios.get("/videos/public").then((res) => res.data.data || []);
}

export function getVideoById(id) {
  return axios.get("/videos/" + id).then((res) => res.data.data || {});
}

export function uploadVideo(file, handleUploadProgress) {
  setAxiosToken(getCookie("token"));
  const fd = new FormData();
  fd.append("video", file);

  return axios.post("/videos", fd, {
    onUploadProgress: handleUploadProgress,
  });
}

export function updateVideo(id, body) {
  setAxiosToken(getCookie("token"));
  return axios.put("/videos/" + id, body).then((res) => res.data.success);
}

export function deleteVideo(id) {
  setAxiosToken(getCookie("token"));
  return axios.delete("/videos/" + id).then((res) => res.data.success);
}
