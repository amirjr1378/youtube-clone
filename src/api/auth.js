import axios from "axios";
import { getCookie } from "../helpers/cookie";
export function login(email, password) {
  const data = { email, password };
  return axios.post("/auth/login", data);
}

// @route   POST /api/v1/auth/updatedetails
export async function changeEmailAndName(email, channelName) {
  const authHeader = {
    Authorization: "Bearer " + getCookie("token"),
  };

  const data = {
    email,
    channelName,
  };
  return axios
    .put("/auth/updatedetails", data, { headers: { ...authHeader } })
    .then((res) => res.data.data);
}

// @route   PUT /api/v1/auth/updatepassword
export async function changePassword(currentPassword, newPassword) {
  const authHeader = {
    Authorization: "Bearer " + getCookie("token"),
  };

  return axios.put(
    "/auth/updatepassword",
    { currentPassword, newPassword },
    { headers: { ...authHeader } }
  );
}

// @route   PUT /api/v1/users
export async function changeAvatar(file, onUploadProgress) {
  const fd = new FormData();
  fd.append("avatar", file);

  const authHeader = {
    Authorization: "Bearer " + getCookie("token"),
  };
  return axios
    .put("/auth/avatar", fd, {
      onUploadProgress: onUploadProgress,
      headers: { ...authHeader },
    })
    .then((res) => res.data.data);
}

export async function getMe() {
  return axios.post("/auth/me").then((res) => res.data.data);
}
