export function truncate(sentence = "", maxLength = 5) {
  return sentence.length < maxLength
    ? sentence
    : sentence.substr(0, maxLength) + "...";
}
