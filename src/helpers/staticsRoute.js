const thumbBaseRoute = "http://localhost:3001/uploads/thumbnails/";
export function makeThumbnailRoute(route) {
  return thumbBaseRoute + route;
}

const videoBaseRoute = "http://localhost:3001/uploads/videos/";
export function makeVideoRoute(route) {
  return videoBaseRoute + route;
}

const avatarBaseRoute = "http://localhost:3001/uploads/avatars/";

export function makeAvatarRoute(route) {
  return avatarBaseRoute + route;
}
