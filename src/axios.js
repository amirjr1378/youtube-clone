import axios from "axios";
import { NotificationManager } from "react-notifications";
import { getCookie } from "./helpers/cookie";
import Router from "next/router";

export function configAxios() {
  const baseURL = "http://localhost:3001/api/v1";
  axios.defaults.baseURL = baseURL;
  // Add a response interceptor
  axios.interceptors.response.use(
    function (response) {
      // Any status code that lie within the range of 2xx cause this function to trigger
      // Do something with response data
      if (response.data.success === true && response.data.message)
        /*show notif */ NotificationManager.success(
          response.data.message,
          "success"
        );
      return response;
    },
    function (error) {
      // Any status codes that falls outside the range of 2xx cause this function to trigger
      // Do something with response error
      console.log("here axios interceptor error", error.message);
      if (!error.response) {
        NotificationManager.error(
          "Please Check Your connection",
          "NETWORK ERROR"
        );
      }
      if (
        error.response &&
        error.response.data.success === false &&
        error.response.data.error
      )
        if (
          Array.isArray(error.response.data.error) &&
          error.response.data.error.length > 0
        ) {
          error.response.data.error.map(
            (e) => e && NotificationManager.error(e, "error")
          );
        } else {
          NotificationManager.error(error.response.data.error, "error");
        }
      return Promise.reject(error);
    }
  );
}

configAxios();

export function setAxiosToken(ctx) {
  const token = getCookie("token", ctx.req);
  console.log("token in axios", token);
  if (!token) {
    typeof window !== "undefined"
      ? Router.push("/login")
      : ctx.res.writeHead(302, { Location: "/login" }).end();

    return console.error("TOKEN is Not Provide");
  }

  const AUTH_TOKEN = `Bearer ${token}`;
  axios.defaults.headers.common["Authorization"] = AUTH_TOKEN;
}

export function makeUploadFunction(callback) {
  return function (progressEvent) {
    var percentCompleted = Math.round(
      (progressEvent.loaded * 100) / progressEvent.total
    );
    callback(percentCompleted);
  };
}
