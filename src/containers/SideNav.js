import { Icon } from "@material-ui/core";
import { HEADER_HEIGHT } from "./Header";
import Link from "next/link";
import { useRouter } from "next/router";
import { useContext } from "react";
import { sidenavContext } from "../context/Sidenav.context";
export const openWidth = "200px";
export const minimizedWidth = "72px";

const routes = [
  {
    href: "/",
    icon: "home",
    label: "Home",
  },
  {
    href: "/my-videos",
    icon: "videocam",
    label: "My Videos",
  },
  {
    href: "/explore",
    icon: "explore",
    label: "explore",
  },
];
// Be sure to include styles at some point, probably during your bootstraping
export default function (props) {
  const router = useRouter();
  const { open } = useContext(sidenavContext);

  return (
    <div className="side-nav">
      {routes &&
        routes.map(({ href, icon, label }) => (
          <Link href={href} key={href}>
            <a
              className={`side-nav__menu-item ${
                router.route === href ? "active" : ""
              }`}
            >
              <div className="side-nav__icon">
                <Icon>{icon}</Icon>
              </div>
              <div className="side-nav__menu-text">{label}</div>
            </a>
          </Link>
        ))}

      <style>{`
    .side-nav {
      height: calc(100vh - ${HEADER_HEIGHT}px);
      background: white;
      transition: width .3s ease;
      width: ${open ? openWidth : minimizedWidth};
      position: fixed;
      left: 0;
      top: ${HEADER_HEIGHT}px;
      display: flex;
      flex-direction: column;
      border-right: 1px solid var(--divider);
    }
    .side-nav__menu-item {
      width: 100%;
      padding: 10px 30px;
      display: flex;
      align-items: center;
      flex-direction: ${open ? "row" : "column"};
      text-align: center;
    }

    .side-nav__menu-item.active {
      background: var(--gray);
      font-weight: bold;
    }

    .side-nav__menu-item:hover {
      background: var(--gray);
      font-weight: bold;
    }

    .side-nav__icon {
      color: var(--red);
      margin-right: 10px;
      margin-top: 5px;
    }
    .side-nav__menu-text {
      margin-right: 10px;
      font-size: ${open ? "1rem" : ".7rem"}
      }
    `}</style>
    </div>
  );
}
