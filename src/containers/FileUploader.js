import { useState } from "react";
import { Icon, Button, Box, LinearProgress } from "@material-ui/core";
import { uploadVideo as uploadVideoToApi } from "../api/videos";
import { makeUploadFunction } from "../axios";

export default function FileUploader({ callback }) {
  const [progress, setProgress] = useState(0);
  const inputRef = React.createRef();
  const openFilePicker = function () {
    inputRef.current && inputRef.current.click();
  };

  const uploadVideo = async function (e) {
    const video = e.target.files[0];
    const handleUploadProgress = makeUploadFunction((percentComplete) =>
      setProgress(percentComplete)
    );

    const res = await uploadVideoToApi(video, handleUploadProgress);
    if (res.data.success) {
      callback(res.data.data);
    }
  };
  return (
    <div className="file-picker">
      <input
        className="file-picker__input"
        type="file"
        ref={inputRef}
        onChange={uploadVideo}
      />
      <div className="file-picker__icon" onClick={openFilePicker}>
        <Icon>publish</Icon>
      </div>

      <div className="file-picker__button">
        <Button variant="contained" color="primary" onClick={openFilePicker}>
          Upload Your Video
        </Button>
      </div>

      {progress > 0 && (
        <Box mt={5}>
          <div className="file-picker__progress">
            <LinearProgress variant="determinate" value={progress} />
          </div>
        </Box>
      )}

      <style jsx>{`
        .file-picker__input {
          display: none;
        }
        .file-picker {
          display: none;
          padding: 1rem;
          display: flex;
          flex-direction: column;
        }
        .file-picker__icon {
          width: 80px;
          height: 80px;
          border-radius: 100%;
          background: lightgray;
          display: flex;
          align-items: center;
          justify-content: center;
          margin: 2rem auto;
          cursor: pointer;
        }
        .file-picker__button {
          display: flex;
          align-items: center;
          justify-content: center;
        }
      `}</style>
    </div>
  );
}
