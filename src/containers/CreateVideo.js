import { useState } from "react";
import FileUploader from "./FileUploader";
import CreateVideoDetails from "./CreateVideoDetails";
import { IconButton, Icon, Divider } from "@material-ui/core";

export default function CreateVideo({ handleClose, video = {} }) {
  // if we doesnt suply video object in props user can upload video
  //otherwise it means user wants to edit
  const isNewVideo = Object.keys(video).length === 0;
  const [uploadStep, setUploadStep] = useState(isNewVideo);
  const [uploadedVideo, setUploadedVideo] = useState({});

  return (
    <div className="upload-video">
      <div className="upload-video__header">
        <div>Upload Videos</div>
        <IconButton onClick={handleClose}>
          <Icon>close</Icon>
        </IconButton>
      </div>
      <Divider />
      {uploadStep ? (
        <FileUploader
          callback={(data) => {
            setUploadedVideo(data);
            setUploadStep(false);
          }}
        />
      ) : (
        <CreateVideoDetails
          video={{ ...video, ...uploadedVideo }}
          callback={handleClose}
        />
      )}

      <style jsx>{`
        .upload-video {
          background-color: white;
          width: 100%;
          max-width: 800px;
          position: fixed;
          top: 20%;
          left: 50%;
          transform: translate(-50%, -20%);
        }
        .upload-video__header {
          display: flex;
          align-items: center;
          justify-content: space-between;
          padding: 0.9rem;
        }
      `}</style>
    </div>
  );
}
