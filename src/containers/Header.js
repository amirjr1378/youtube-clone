import { useState } from "react";
import Icon from "@material-ui/core/Icon";
import { useRouter } from "next/router";
import {
  IconButton,
  Avatar,
  Menu,
  MenuItem,
  Divider,
  Modal,
} from "@material-ui/core";
import CreateVideo from "./CreateVideo";
import { useContext } from "react";
import { userContext } from "../context/user.context";
import { sidenavContext } from "../context/Sidenav.context";
import HamburgerMenu from "react-hamburger-menu";
import { makeAvatarRoute } from "../helpers/staticsRoute";
import { getMe } from "../api";
import { setAxiosToken } from "../axios";
import { getCookie } from "../helpers/cookie";

export const HEADER_HEIGHT = 80;

function Header() {
  const router = useRouter();
  const { logout } = useContext(userContext);
  const { toggle, open } = useContext(sidenavContext);

  const [text, setText] = useState("");
  const handleSearch = function (e) {
    e.preventDefault();
    router.push(`/search?text=${text}`);
  };

  const [anchorEl, setAnchorEl] = useState(null);

  function showProfileMenu(e) {
    setAnchorEl(e.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  const [createModalVisibility, setCreateModalVisibility] = useState(false);
  function showCreateVideo() {
    setCreateModalVisibility(true);
  }
  function closeCreateVideo() {
    setCreateModalVisibility(false);
  }

  const [user, setUser] = useState({});
  React.useEffect(() => {
    setAxiosToken(getCookie("token"));
    getMe().then((me) => setUser(me));
  }, []);

  return (
    <div className="header">
      <div className="header__toggle">
        <HamburgerMenu
          width={18}
          height={15}
          isOpen={open}
          menuClicked={toggle}
        />
      </div>
      <div className="header__logo" onClick={() => router.push("/")}>
        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQgAAAC/CAMAAAA1kLK0AAAAw1BMVEX///8AAADgChTeAADNy8tRT08sKCb+/P3j4uHFw8PPzs2joqH5+fiRkY8WExExLi775+Lc3NtJRUV7eHfw7u788vLBv77vlpbgAA5hXl7W1dThAAB+fnz3zsv//Pf2wr5OTEu1tLNjYmGGhIMcGxomIiChn5/08/J0cnFBPj3lREL62tb4zsvztrHwq6XwpZ/qfHjnW1DjMCjlPTjpbmnnW1fuk47hHB3rh4TlT0uurazocm30u7WWlZTjMzEcGhXoYl6TTIACAAAGsklEQVR4nO2aaV+qTBiH4ZCJqaWFhli5VWiWVp42y07f/1M9zD4oKIG0PL//9eYAh8Z7Lma5Z8AwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAOGvUxuPr687bzc1kOj2WTKeTm5u3zvXpeFxrnH13lDlS6xzf3t23raTc390ed2rfHfXW6cxI5Vz3T2Jcl/zFrPO1cdYLrYBCPa/y/1qfUBDSYf1V3cS3Kb44Z6d2kgD4n4ZwIu7bMSl72ascyYOVTgPBepDF2CzKk/DpMEkEZXOVQcR9+Yp4zOAhMPEoC9qlUR7wsyYLupkkhCgR1Yj7chXRSNsvhImGKGnAwuRnVXbmx/xsiB8hYpqpQQQiptFhjuhJIVEMP0LEXVYRL7IoFmaZHhfZyWWiGH7EGNHO1jP+uE9y4ijQMNnw6LGYk810dpNQYo3ogp40o/4yTxG1jA0iaBIyr7pkFaHH5dCAkQwmcjf+hjxFnGYXcSrKqmvj41BrHUnhIoqxN+Qp4ia7iIksbJ/GWSKHLOTep2L5VhHH8SISzqvWsSzsSI73Pgs5Kj2M51tFPLdjK9h5SaTCepaFsSSqJY94cmWXjwrd1slcaqkzyKHDDv0lEfpldmiHRNiLVnc41+rh9wb9buvoMvU65DW2RQSD4NtTgo7T/isLc2ic+xWRTtFcwGktT4kVdnZOjtkAyxJQTQRPx0rq7kNdBM/ddmW6VpW/0f1cK5TELzTIbHD2aG1U4apEgmdRtqiTZ8g1B4+SNvvKvmovZVXj9SIKSoQ3FOUd8t9Vrj/dHwWz2NbPpsXaxqWp+65KW9BIemKsJNXY1WNkS7KsIoaqPPp3WnugtlOJeNogIphgZ+tVuPeqNI/XljWDvlSjsLcgQoP8BO+RQZ/k/6YaTOMTS5UoTdrr+of7pEpjVTw0ejQgMpix4M5tfokOE5lFHNYdNkGZ5xUp+9wxnAv+IFKwZvbUduKe1w4VWnF9GgkfzIKnv8fiJfkE78dbEEFzz3NT/IZxQI/IskbPbXMRYdQ+YvuH62qbuXMWXUFUlPcMRwZJIs8qgqZpfKDYkTnLjrohPhPJLMIwrt9jVIREsO7ao4+L9IK+bAZGSdYtqwg6BlxKKdr6zlbNJDcRhjGNGVjb+vY+baYn8hGxVvuP/A/vJeUtiZjL4pqq9nzY9PIVEbepFxJBHbCKkmyHBUY7NX9w1S2J4NW/lEU46t4000ZyEafvMfeGRPAOYLJMO0LEyZZElKTX6peKiB8tQ2OE2Jky2eRZXBVxtCUR/HjwtSKKa+ZP1w0VWBAibBVYjiJO5L227/tOziJu3LUrjlCBfDRnlf9KERppRPzZnFmOr9bm2HpmachtKr6q+D0i7jeJaLxuWHXpaw2CqVXtF4l42SBi8zrcnYVL5Clf5ZeJ+Fi7H9FJsDOjvf6k8OWQ8dUi6o6kkkLEbbyIxjjRXp31Gi8ixzwicvrMwHO8iI/Nu1P0vsdwiRtEDLabWS5CC7sMrHn1mXCjX738TCRisfW1Bj+iK615udfcS7WB28n+XuNtjYiROlFr6C2vPnc0EWyRl8bEOLuI8RoR2hTSk7XYighesiczF3pVc/JJGtlFNMIlhkSUZbhiCz5wUmE7avvkhkW0iIV83mLfZVkE3xR2DP2tGpeW6KOMZeJ3b5Ohb2KviuBPi+zRsGYwIldHskJ8k2JFBG8+ZG+2tSJi5EvDtFX15Q1pXj0L4ufPhA1iafYMizDYwzdP5rzycxV5sIaW7yOWRfCR1ewPxNZ0aBe7y0tjiTyfQFpltiFEUpUUZB0treVPDMMiemYYmuvMtQvDSBFF7Y6jVRESthcVfneSaogIeM/UN1Z6xpKI8Eso/kKmoi7Mm5EixIhikta/JGKkcuo++409U2eRzkPGLyTU1xExIip9LUbxmUBTXKjyTkBFdOnhBXsteCA8OGxI6UoRQzFjmBf+cnlm0q+Vopik/67O1T6OEFRHhwEH6kJJNIqBSv5KdOwY7QQ1JreP2IxI/7LF7vCH3BQrcUS+OfEOyL3NoOLUzVCtKRzRSgYp+wXl1LXSfEnlWlb7Otkv2J7nOSuXNiTFFc+Lr5Vfry+9vXDqXj1jmh00ig/1vblLiaw5Rd35sNoc/gc0xp3J8fPrx8PL1ez+yW23lz7VDy64T/ezq5eHj9fnx0ln3Nhc5q/nrFhsRFBM8yoNAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPBj+A8dPI2jPLoMxwAAAABJRU5ErkJggg==" />
      </div>
      <div className="header__search">
        <form onSubmit={handleSearch} className="header__search__container">
          <input
            type="text"
            placeholder="search"
            onChange={(e) => setText(e.target.value)}
          />
          <button type="submit" className="header__search__icon">
            <Icon>search</Icon>
          </button>
        </form>
      </div>
      <div className="header__actions">
        <IconButton onClick={showCreateVideo}>
          <Icon>video_call</Icon>
        </IconButton>

        <Avatar
          alt={user.channelName}
          src={makeAvatarRoute(user.photoUrl)}
          onClick={showProfileMenu}
        />
        <Menu
          id="profile actions"
          anchorEl={anchorEl}
          keepMounted
          open={!!anchorEl}
          onClose={handleClose}
        >
          <MenuItem onClick={() => router.push("/profile")}>
            my chanell
          </MenuItem>
          <Divider />
          <MenuItem onClick={logout}>logout</MenuItem>
        </Menu>
      </div>

      <Modal open={createModalVisibility} onClose={closeCreateVideo}>
        <CreateVideo handleClose={closeCreateVideo} />
      </Modal>

      <style jsx>{`
        .header {
          width: 100vw;
          padding: 0 10px;
          overflow: hidden;
          border-bottom: 1px solid var(--divider);
          background: white;
          display: flex;
          align-items: center;
          height: ${HEADER_HEIGHT}px;
          position: fixed;
          left: 0;
          top: 0;
          z-index: 2;
        }
        .header__logo {
          width: 100px;
        }
        .header__logo img {
          object-fit: contain;
          width: 100%;
          height: 100%;
        }

        .header__search {
          flex-grow: 1;
          display: flex;
          justify-content: center;
        }
        .header__search__container {
          max-width: 640px;
          width: 100%;
          display: flex;
        }
        .header__search__container input {
          padding: 5px 10px;
          color: var(--text-gray);
          flex-grow: 1;
          border: 1px solid var(--divider);
          border-radius: 4px;
          border-top-right-radius: 0;
          border-bottom-right-radius: 0;
          font-size: 1rem;
        }
        @media (max-width: 800px) {
          .header__search {
            display: none;
          }
          .header__logo {
            margin: auto;
          }
        }
        .header__search__container button {
          color: var(--text-gray);
          background-color: var(--gray);
          padding: 5px 30px;
          margin-left: -5px;
          border: 1px solid var(--divider);
          background-color: transparent;
          border-left: 0px;
          cursor: pointer;
          border-radius: 4px;
        }

        .header__actions {
          display: flex;
          padding: 0 10px;
          align-items: center;
        }
        .header__toggle {
          margin-top: -6px;
          cursor: pointer;
        }
      `}</style>
    </div>
  );
}

export async function getStaticProps() {
  let me = {};

  try {
    me = await getMe();
  } catch {
    me = {};
  }

  return {
    props: {
      user: me,
    },
  };
}

export default Header;
