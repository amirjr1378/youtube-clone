import { useState } from "react";
import {
  TextField,
  Button,
  Box,
  LinearProgress,
  Select,
} from "@material-ui/core";
import Axios from "axios";
import UploadThumbnail from "../components/UploadThumbnail";
import { updateVideo, deleteVideo } from "../api/videos";
import LoadingButton from "../components/LoadingButton";

export default function CreateVideoDetails({
  callback,
  video = {
    title: "",
    description: "",
    status: "public",
    _id: "",
  },
  ...props
}) {
  const [title, setTitle] = useState(video.title);
  const [description, setDescription] = useState(video.description);
  const [status, setStatus] = useState(video.status);

  const [loading, setLoading] = useState(false);

  const handleSubmit = async function (e) {
    e.preventDefault();
    setLoading(true);
    const data = {
      title,
      description,
      status,
    };
    console.log("status", status);
    try {
      const success = await updateVideo(video._id, data);
      console.log("success", success);
      if (success === true) {
        callback && callback();
      }
    } catch (e) {
      console.log("error", e);
    } finally {
      setLoading(false);
    }
  };

  const handleDelete = async function () {
    //delete video
    try {
      setLoading(true);
      const success = await deleteVideo(video._id);
      if (success) {
        callback && callback();
      }
    } catch (e) {
      console.log("error", e.message);
    } finally {
      setLoading(false);
    }
  };
  return (
    <div className="video-details">
      <form onSubmit={handleSubmit} className="video-details__form">
        <TextField
          variant="outlined"
          id="title"
          label="Title"
          name="title"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          required
          style={{ margin: "1rem" }}
        />

        <TextField
          variant="outlined"
          id="description"
          label="Description"
          name="description"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          required
          style={{ margin: "1rem" }}
        />
        <Select
          variant="outlined"
          native
          value={status}
          onChange={(e) => setStatus(e.target.value)}
          required
          style={{ margin: "1rem" }}
        >
          <option value="public">public</option>
          <option value="private">private</option>
          <option value="draft">draft</option>
        </Select>

        <UploadThumbnail id={video._id} label="Upload Thumbnail" />

        <LoadingButton
          loading={loading}
          variant="contained"
          color="primary"
          type="submit"
          style={{ margin: "0.5rem" }}
        >
          save
        </LoadingButton>

        <LoadingButton
          loading={loading}
          onClick={handleDelete}
          variant="contained"
          color="secondary"
          style={{ margin: "0.5rem" }}
        >
          Delete
        </LoadingButton>
      </form>
      <style jsx>{`
        .video-details {
          padding: 1rem;
        }

        .video-details__form {
          display: flex;
          flex-direction: column;
        }
      `}</style>
    </div>
  );
}
