import Header from "./Header";
import SideNav, { openWidth, minimizedWidth } from "./SideNav";
import { useContext } from "react";
import { sidenavContext } from "../context/Sidenav.context";

export default function MainLayout({ children }) {
  const { open, toggle } = useContext(sidenavContext);
  return (
    <div className="main-layout">
      <Header toggle={toggle} />
      <div className="offset-top"></div>
      <SideNav />
      <div className="main-layout__content">
        <div className="offset-x"></div>
        <div className="main-layout__seed">{children}</div>
      </div>
      <style jsx>{`
        .main-layout {
          display: flex;
          flex-direction: column;
        }
        .offset-top {
          margin-top: 85px;
        }
        .main-layout__content {
          display: flex;
          width: 100%;
          padding: 20px;
        }

        .offset-x {
          transition: all 0.3s ease;
          flex-basis: calc(${open ? openWidth : minimizedWidth});
        }
        .main-layout__seed {
          transition: all 0.3s ease;
          flex-grow: 1;
          box-sizing: border-box;
          padding: 1.2em 1em 1.2em 0;
        }
      `}</style>
    </div>
  );
}
