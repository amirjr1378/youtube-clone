const fs = require("fs");
const mongoose = require("mongoose");
const colors = require("colors");

const User = require("./models/User");
const { MONGO_URI } = require("./config/variables");

mongoose.connect(MONGO_URI, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true,
});

const users = JSON.parse(fs.readFileSync(`./_data/users.json`, "utf-8"));
const importData = async () => {
  try {
    await User.create(users);

    process.exit();
  } catch (err) {
    console.log("errrrrrrrrrrrrorjorjorjor");
  }
};

const deleteData = async () => {
  try {
    await User.deleteMany();

    console.log("Data Destroyed...".red.inverse);
    process.exit();
  } catch (err) {
    console.error(err);
  }
};

if (process.argv[2] === "-i") {
  // node seeder -i
  importData();
} else if (process.argv[2] === "-d") {
  // node seeder -d
  deleteData();
}
