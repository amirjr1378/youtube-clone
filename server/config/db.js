const mongoose = require("mongoose");
const { MONGO_URI } = require("./variables");
const DBconnection = async () => {
  const conn = await mongoose
    .connect(MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    })
    .catch((err) => {
      console.log(`For some reasons we couldn't connect to the DB`.red, err);
    });

  console.log(`MongoDB Connected: ${conn.connection.host}`.cyan.underline.bold);
};

module.exports = DBconnection;
