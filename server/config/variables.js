module.exports.NODE_ENV = "development";
module.exports.PORT = 3001;

module.exports.MONGO_URI =
  "mongodb+srv://amirjr:amirjr1378@youtube-clone.r1ooi.mongodb.net/youtube-clone?retryWrites=true&w=majority";

module.exports.JWT_SECRET = "AMIR_YOUTUBE_CLONE";
module.exports.JWT_EXPIRE = "30d";
module.exports.JWT_COOKIE_EXPIRE = 30;

module.exports.FILE_UPLOAD_PATH = "./server/public/uploads";
module.exports.MAX_FILE_UPLOAD = 1000000;

module.exports.SMTP_HOST = "smtp.mailtrap.io";
module.exports.SMTP_PORT = 2525;
module.exports.SMTP_EMAIL = "";
module.exports.SMTP_PASSWORD = "";
module.exports.FROM_EMAIL = "noreply@boilerplate.com";
module.exports.FROM_NAME = "Boilerplate";
