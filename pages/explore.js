import MainLayout from "../src/containers/MainLayout";
import { getAllVideos } from "../src/api/videos";
import VideoPriview from "../src/components/VideoPriview";
import { Grid } from "@material-ui/core";

export default function Explore({ videos, ...props }) {
  return (
    <MainLayout>
      <h2>Explore people videos</h2>
      <Grid container className="p-3" spacing={3}>
        {videos.length !== 0 ? (
          videos.map((video) => (
            <Grid item xs={12} sm={6} md={4} lg={3} key={video.id}>
              <VideoPriview {...video} />
            </Grid>
          ))
        ) : (
          <h2 style={{ margin: 20 }}> doesnt have any videos to show</h2>
        )}
      </Grid>
    </MainLayout>
  );
}

export async function getStaticProps() {
  let videos = [],
    error = null;
  try {
    videos = await getAllVideos();
  } catch (e) {
    error = e.message;
    videos = [];
  }
  return {
    props: { videos, error },
  };
}
