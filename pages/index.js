import MainLayout from "../src/containers/MainLayout";
import VideoPriview from "../src/components/VideoPriview";
import { setAxiosToken } from "../src/axios";
import { Grid } from "@material-ui/core";
import { getSubVideos } from "../src/api/videos";

export default function Home(props) {
  const { videos } = props;
  return (
    <MainLayout>
      <h2>your subscription videos</h2>
      <Grid container className="p-3" spacing={3}>
        {videos.length !== 0 ? (
          videos.map((video) => (
            <Grid item xs={12} sm={6} md={4} lg={3} key={video.id}>
              <VideoPriview {...video} />
            </Grid>
          ))
        ) : (
          <h2 style={{ margin: 20 }}> doesnt have any videos to show</h2>
        )}
      </Grid>
    </MainLayout>
  );
}

export const getServerSideProps = async (ctx) => {
  setAxiosToken(ctx);
  let videos = [];

  try {
    videos = await getSubVideos();
  } catch (e) {
    videos = [];
  }
  return {
    props: { videos },
  };
};
