import { useState } from "react";
import MainLayout from "../src/containers/MainLayout";
import { Grid, TextField, Button, Avatar } from "@material-ui/core";
import Card from "../src/components/Card/Card";
import CardBody from "../src/components/Card/CardBody";
import UploadImage from "../src/components/UploadImage";
import {
  getMe,
  changeEmailAndName,
  changePassword,
  changeAvatar,
} from "../src/api/auth";
import { makeAvatarRoute } from "../src/helpers/staticsRoute";
import { makeUploadFunction } from "../src/axios";

const marginBottom = {
  marginBottom: 10,
};
function profile({ user: initUser }) {
  const [user, setUser] = useState(initUser);
  const [progress, setProgress] = useState(0);
  async function handleEditNameOrName(e) {
    e.preventDefault();
    const { email, name } = e.target.elements;
    try {
      const newUser = await changeEmailAndName(email.value, name.value);
      console.log("new user ", newUser);
      setUser(newUser || {});
    } catch (e) {
      console.log("error", e);
    }
  }

  async function handleEditPass(e) {
    e.preventDefault();
    const { currentPassword, newPassword } = e.target.elements;
    try {
      const newUser = await changePassword(
        currentPassword.value,
        newPassword.value
      );
    } catch (e) {
      console.log("error", e);
    }
  }

  async function handleUploadAvatar(file) {
    console.log("file", file);
    const handleUploadProgress = makeUploadFunction((pr) => setProgress(pr));

    const photoUrl = await changeAvatar(file, handleUploadProgress);
    setUser((state) => ({ ...state, photoUrl }));
    setProgress(0);
  }
  return (
    <MainLayout>
      <div className="channel-details">
        <Avatar src={makeAvatarRoute(user.photoUrl)} alt={user.channelName} />
        <h3>"{user.channelName}"</h3>
      </div>
      <Grid container spacing={4}>
        <Grid item xs={12} lg={6}>
          <Card title="Edit email or name">
            <form onSubmit={handleEditNameOrName}>
              <CardBody>
                <TextField
                  label="email"
                  name="email"
                  variant="outlined"
                  style={marginBottom}
                  defaultValue={user.email}
                />
                <TextField
                  label="Channel Name"
                  name="name"
                  variant="outlined"
                  style={marginBottom}
                  defaultValue={user.channelName}
                />

                <Button type="submit" variant="contained" color="primary">
                  save
                </Button>
              </CardBody>
            </form>
          </Card>
        </Grid>

        <Grid item xs={12} lg={6}>
          <form onSubmit={handleEditPass}>
            <Card title="Edit Pass">
              <CardBody>
                <TextField
                  label="current password"
                  name="currentPassword"
                  variant="outlined"
                  type="text"
                  style={marginBottom}
                />
                <TextField
                  label="new password"
                  name="newPassword"
                  variant="outlined"
                  type="password"
                  autoComplete="new-password"
                  style={marginBottom}
                />

                <Button type="submit" variant="contained" color="primary">
                  save
                </Button>
              </CardBody>
            </Card>
          </form>
        </Grid>

        <Grid item xs={12}>
          <Card title="Edit profile photo">
            <CardBody>
              <UploadImage
                name="profilePhoto"
                callback={handleUploadAvatar}
                progress={progress}
              />
            </CardBody>
          </Card>
        </Grid>
      </Grid>

      <style jsx>{`
        .channel-details {
          display: flex;
          align-items: center;
          justify-content: center;
          margin: 20px 0;
        }
      `}</style>
    </MainLayout>
  );
}

export async function getStaticProps() {
  let me = {};

  try {
    me = await getMe();
  } catch {
    me = {};
  }

  return {
    props: {
      user: me,
    },
  };
}

export default profile;
