import { setAxiosToken } from "../src/axios";
import { search } from "../src/api/search";
import MainLayout from "../src/containers/MainLayout";
import { Grid } from "@material-ui/core";
import SearchUser from "../src/components/SearchUser";
import SearchVideo from "../src/components/SearchVideo";

export default function Search({ results }) {
  return (
    <MainLayout>
      {results && results.length === 0 ? (
        <h2>nothing found</h2>
      ) : (
        <Grid container>
          {results.map((result) => {
            if ("videos" in result)
              //if it is user
              return (
                <Grid item xs={12}>
                  <SearchUser {...result} />
                </Grid>
              );

            return (
              <Grid item xs={12}>
                <SearchVideo {...result} />;
              </Grid>
            );
          })}
        </Grid>
      )}
    </MainLayout>
  );
}

export async function getServerSideProps(ctx) {
  setAxiosToken(ctx);
  let results = [],
    error = "";
  const { text } = ctx.query;
  try {
    results = (await search(text)) || [];
  } catch (e) {
    results = [];
    error = e.message;
  }

  return {
    props: {
      results,
      error,
    },
  };
}
