import MainLayout from "../../src/containers/MainLayout";
import { getAllVideos, getVideoById } from "../../src/api/videos";
import { setAxiosToken } from "../../src/axios";
import { getCookie } from "../../src/helpers/cookie";

export default function VideoPage({ ...props }) {
  return <MainLayout>video</MainLayout>;
}

export async function getStaticPaths() {
  let videos = [];
  try {
    videos = await getAllVideos();
  } catch (e) {
    console.log("error", e);
  }
  const paths = videos.map((video) => ({
    params: {
      id: video.id.toString(),
    },
  }));
  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps({ params }) {
  const { id } = params;
  let video = {},
    error = null;
  try {
    video = await getVideoById(id);
  } catch (e) {
    error = e;
    video = {};
  }
  return {
    props: { video },
  };
}
