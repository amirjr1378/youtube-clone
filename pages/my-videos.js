import { useState } from "react";
import { Grid, Modal } from "@material-ui/core";
import MainLayout from "../src/containers/MainLayout";
import VideoPriview from "../src/components/VideoPriview";
import axios from "axios";
import Cookies from "js-cookie";
import { setAxiosToken } from "../src/axios";
import CreateVideo from "../src/containers/CreateVideo";
// import { parseCookies } from "../src/helpers/parseCookies";

export default function MyVideos(props) {
  const { videos } = props;

  const [editModalVisibility, setEditModalVisibility] = useState(false);
  const [video, setVideo] = useState({});
  function showEditVideo(payload) {
    setVideo(payload);
    setEditModalVisibility(true);
  }
  function closeEditVideo() {
    setEditModalVisibility(false);
  }
  return (
    <MainLayout>
      <h2>browse and edit your own videos</h2>
      <Grid container className="p-3" spacing={3}>
        {videos !== undefined && videos.length !== 0 ? (
          videos.map((video) => (
            <Grid item xs={12} sm={6} md={4} lg={3} key={video.id}>
              <VideoPriview {...video} onClick={() => showEditVideo(video)} />
            </Grid>
          ))
        ) : (
          <h2 style={{ margin: 20 }}>no video yet</h2>
        )}
      </Grid>

      <Modal open={editModalVisibility} onClose={closeEditVideo}>
        <CreateVideo handleClose={closeEditVideo} video={video} />
      </Modal>
    </MainLayout>
  );
}

export async function getStaticProps() {
  let videos = [],
    error = null;
  try {
    const res = await axios.get("/videos/private");
    videos = res.data.data;
  } catch (e) {
    console.log("error", e);
    videos = [];
    error = e.message;
  }

  return {
    props: { videos, error },
  };
}
